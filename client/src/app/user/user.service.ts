import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { concatMap, tap } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import 'firebase/messaging';

@Injectable()
export class UserService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService,
  ) {
  }

  public initializeFirebase() {
    if (!firebase.apps.length) {
      firebase.initializeApp(ApiConfig.fcm);
    }
  }

  /**
   * Updates the profile of the current user.
   * @param profile Profile data
   */
  public updateProfile(profile: any): Observable<any> {
    return this.http.put(
      ApiConfig.url + '/api/profile',
      JSON.stringify(profile)
    );
  }

  public updateAvatar(avatarData: File, cropData = null, userId = ''): Observable<any> {
    const uploadData: FormData = new FormData();
    uploadData.append('avatar', avatarData);

    if (userId) {
      uploadData.append('id', userId);
    }

    if (cropData) {
      uploadData.append('crop', JSON.stringify(cropData));
    }

    return this.http.post(
      ApiConfig.url + '/api/profile/avatar/upload', uploadData, {
        headers: { 'X-SKIP-HEADERS': 'true' }
      }
    );
  }

  public deleteProfile(): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/profile');
  }

  /**
   * Gets the complete profile of the given id or, if null, the
   * currently logged in user.
   */
  public getProfile(id: string = null): Observable<any> {
    return this.http.get(
      ApiConfig.url + '/api/profile' + (id ? '/' + id : '')
    );
  }

  /**
   * Gets the complete profile of the given username.
   */
  public getProfileByUsername(username: string): Observable<any> {
    return this.http.get(
      ApiConfig.url + '/api/profile/username/' + username
    );
  }

  public getProfileStats(usernameOrId: string, monthOffset: number = -1): Observable<any> {
    return this.http.get(
      ApiConfig.url + '/api/profile/stats/' + usernameOrId + (monthOffset >= 0
        ? '/' + monthOffset.toString()
        : '')
    );
  }

  public getAll(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/user');
  }

  public search(term: string): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/user/search',
      JSON.stringify({ term })
    );
  }

  public getOnlineCount(): Observable<any> {
    return this.wsService.getSocket().pipe(
      tap(websocket => websocket.emit('onlinecounter:get')),
      concatMap(websocket => Observable.create(obs => {
        websocket.on('onlinecounter:get', res => obs.next(res));
      }))
    );
  }

  public getOnlineUsers(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/user/online');
  }

  public getOnlineSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('onlinecounter:changed', res => obs.next(res));
      }))
    );
  }

  public get(id: string): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/user/' + id);
  }

  public save(user): Observable<any> {
    const method = user._id ? 'put' : 'post';

    return this.http[method](
      ApiConfig.url + '/api/user' + (user._id ? '/' + user._id : ''),
      user
    );
  }

  public delete(user): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/user/' + user._id);
  }

  public deleteAvatar(userId: string): Observable<any> {
    return this.http.delete(ApiConfig.url + '/api/profile/avatar/' + userId);
  }

  public registerPushSubscription(token: string, oldToken: string = ''): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/push/register',
      JSON.stringify({ token, oldToken })
    );
  }

  public getUsername(search: string): Observable<any> {
    return this.http.get(
      ApiConfig.url + '/api/user/mention/' + search
    );
  }
}
