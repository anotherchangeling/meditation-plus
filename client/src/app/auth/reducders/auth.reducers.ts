import * as auth from '../actions/auth.actions';
import { AppState } from 'app/reducers';
import { createSelector } from '@ngrx/store';
import { AuthService } from '../../shared/auth.service';
import { Profile } from 'app/profile/profile';
import * as moment from 'moment';

export interface AuthState {
  token: string;
  tokenExpires: number;
  profile: Profile | null;
  loading: boolean;
  error: string;
}

export const initialAuthState: AuthState = {
  token: '',
  profile: null,
  tokenExpires: 0,
  loading: false,
  error: ''
};

export function authReducer(
  state = initialAuthState,
  action: auth.Actions
): AuthState {
  // fallback for old localStorages
  if (state.token && !state.profile) {
    return initialAuthState;
  }

  switch (action.type) {
    case auth.LOGIN: {
      return { ...state, loading: true, error: '' };
    }
    case auth.LOGIN_ERROR: {
      return { ...state, loading: false, error: action.payload };
    }
    case auth.LOGIN_DONE: {
      const profile = {
        ...action.payload.profile,
        lastLike: moment(action.payload.profile.lastLike)
      };

      return {
        ...state,
        error: '',
        loading: false,
        token: action.payload.token,
        tokenExpires: action.payload.tokenExpires,
        profile
      };
    }
    case auth.RELOAD_PROFILE_DONE: {
      const profile = {
        ...action.payload,
        lastLike: moment(action.payload.lastLike)
      };
      return {
        ...state,
        profile
      };
    }
    case auth.LOGOUT: {
      return { ...state, loading: true, error: '' };
    }
    case auth.LOGOUT_DONE: {
      return { ...initialAuthState };
    }
    case auth.LOGOUT_ERROR: {
      return { ...state, loading: false, error: action.payload };
    }
    case auth.SIGNUP: {
      return { ...state, loading: true, error: '' };
    }
    case auth.SIGNUP_DONE: {
      return { ...state, loading: false, error: '' };
    }
    case auth.SIGNUP_ERROR: {
      return { ...state, loading: false, error: action.payload };
    }

    default: {
      return state;
    }
  }
}


// Selectors for easy access
export const selectAuth = (state: AppState) => state.auth;

export const selectToken = createSelector(selectAuth, (state: AuthState) => state.token);
export const selectId = createSelector(selectAuth,
  (state: AuthState) => state.profile ? state.profile._id : null
);
export const selectRole = createSelector(selectAuth,
  (state: AuthState) => state.profile ? state.profile.role : null
);
export const selectTokenExpires = createSelector(selectAuth, (state: AuthState) => state.tokenExpires);
export const selectLoading = createSelector(selectAuth, (state: AuthState) => state.loading);
export const selectError = createSelector(selectAuth, (state: AuthState) => state.error);
export const selectProfile = createSelector(selectAuth, (state: AuthState) => state.profile);
export const selectAdmin = createSelector(selectAuth,
  (state: AuthState) => state.profile && state.profile.role && state.profile.role === AuthService.adminRole
);
export const selectTokenExpired = createSelector(selectTokenExpires, exp => {
  const date = new Date(0);
  date.setUTCSeconds(exp);
  if (date === null) {
    return true;
  }

  return !(date.valueOf() > new Date().valueOf());
});
