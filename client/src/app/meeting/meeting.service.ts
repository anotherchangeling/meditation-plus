import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class MeetingService {
  public constructor(
    private http: HttpClient
  ) { }


  public getMeeting(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/meeting');
  }

  public initiate(): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/meeting/initiate', '');
  }

  public close(meetingId: string): Observable<any> {
    return this.http.post(ApiConfig.url + '/api/meeting/close', JSON.stringify({ meetingId }));
  }
}
