import * as moment from 'moment';

export const MEDITATION_CHART_OPTIONS = {
  animations: true,
  maintainAspectRatio: false,
  responsive: true,
  legend: false,
  scales: {
    yAxes: [{
      display: false
    }],
    xAxes: [{
      // needed to have both series (normal and current hour) in one place
      stacked: true,
      gridLines: {
        display: false
     },
    }]
  },
  tooltips: {
    callbacks: {
      label: formatTooltipLabel,
      title: formatTooltipTitle
    }
  }
};

function formatTooltipTitle(tooltipItem) {
  const value: string = tooltipItem[0].xLabel;
  return value.length === 2 ? `${value}:00 UTC` : `0${value}:00 UTC`;
}

function formatTooltipLabel(tooltipItem) {
  const value: number = tooltipItem.yLabel;
  if (!value) {
    return;
  }

  const duration = moment.duration(value, 'minutes');
  const hours = duration.asHours();

  return hours >= 24 ? Math.floor(hours) + ' hours' : duration.humanize();
}
