import { TestHelper } from '../../../testing/test.helper';

export class FakeAuthService {
  public static tokenName = 'token';
  public static adminRole = 'ROLE_ADMIN';

  public constructor() {
  }

  public getToken() {
    return TestHelper.noResponse();
  }

  public setToken(token: string) {
    return TestHelper.noResponse();
  }

  public removeToken() {
    return TestHelper.noResponse();
  }

  /**
   * FIXME: test
   */
  public getUserId() {
    return TestHelper.noResponse();
  }

  /**
   * Checks whether the currently logged in user is an admin.
   */
  public isAdmin() {
    return TestHelper.noResponse();
  }

  /**
   * Check if the local access token is expired
   */
  public isTokenExpired() {
    return TestHelper.noResponse();
  }

  /**
   * Logging in via email and password.
   */
  public login(
    email: string,
    password: string,
    username?: string,
    acceptGdpr?: boolean,
    deleteAccount?: boolean
  ) {
    return TestHelper.noResponse();
  }

  /**
   * Verify account by sending token received via email to server
   */
  public verify(token: string) {
    return TestHelper.noResponse();
  }

  /**
   * Resend email activation with token
   * @param email mail address of user
   */
  public resend(email: string) {
    return TestHelper.noResponse();
  }

  public resetPasswordInit(email: string) {
    return TestHelper.noResponse();
  }

  public resetPassword(userId: string, token: string, newPassword: string) {
    return TestHelper.noResponse();
  }

  /**
   * Register refresh subscription
   */
  public registerRefresh() {
    return TestHelper.noResponse();
  }


  public signup(name: string, password: string, email: string, username: string, acceptedGdpr: boolean) {
    return TestHelper.noResponse();
  }

  /**
   * Logging out the current user. Removes the token from localStorage.
   */
  public logout() {
    return TestHelper.noResponse();
  }
}
