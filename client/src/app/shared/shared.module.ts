import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkyModule } from 'angular-linky';
import { WebsocketService } from './websocket.service';
import { MaterialModule } from './material.module';
import { AuthService } from './auth.service';
import { AdminGuard } from './admin-guard';
import { LoginGuard } from './login-guard';
import { AuthGuard } from './auth-guard';
import { FormatHourPipe } from './hour.pipe';
import { ErrorHandlerService } from './error-handler.service';
import { AvatarDirective } from '../profile/avatar.directive';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    LinkyModule
  ],
  providers: [
    WebsocketService,
    ErrorHandlerService,
    AuthService,
    AuthGuard,
    LoginGuard,
    AdminGuard
  ],
  declarations: [
    FormatHourPipe,
    AvatarDirective
  ],
  exports: [
    CommonModule,
    MaterialModule,
    LinkyModule,
    FormatHourPipe,
    AvatarDirective
  ]
})
export class SharedModule { }
