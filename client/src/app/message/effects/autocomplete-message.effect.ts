import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { AutocompleteUser, AUTOCOMPLETE_USER } from 'app/message/actions/message.actions';
import { map, withLatestFrom, concatMap, filter } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers';
import { selectUsernames } from 'app/message/reducers/message.reducers';
import { UserService } from 'app/user';

@Injectable()
export class AutocompleteMessageEffect {
  constructor(
    private actions$: Actions,
    private store$: Store<AppState>,
    private userService: UserService
  ) {
  }

  /**
   * The MessageComponent is listening on this observable
   * to update its input field.
   */
  @Effect({ dispatch: false })
  $autocomplete = this.actions$
    .ofType<AutocompleteUser>(AUTOCOMPLETE_USER)
    .pipe(
      map(action => action.payload),
      withLatestFrom(this.store$.select(selectUsernames)),
      concatMap(([payload, usernames]) =>
        mapAutocompleteToString(payload.cursorPosition, usernames, payload.message, this.userService)
      )
    );
}

export function mapAutocompleteToString(
  caretPos: number,
  usernames: string[],
  curMsg: string,
  userService: UserService
): Observable<string|null> {

  const textBfCaret = curMsg.substring(0, caretPos);
  const search = textBfCaret.match(/@\w+$/g);

  if (!search || search.length === 0) {
    return null;
  }

  const matches = usernames
    .filter(name => new RegExp('^' + search[0].substring(1), 'i').test(name));

  if (matches.length > 0) {
    return of(createAutocompletePayload(
      caretPos, curMsg, textBfCaret,
      search, matches[0]
    ));
  } else {
    return userService.getUsername(search[0].substring(1))
      .pipe(
        filter(res => res.length > 0),
        map(username => createAutocompletePayload(
          caretPos, curMsg, textBfCaret,
          search, username
        ))
      );
  }
}

export function createAutocompletePayload(
  caretPos: number,
  curMsg: string,
  textBfCaret: string,
  search: any[],
  username: string
): string {
  textBfCaret = textBfCaret.slice(0, 1 - search[0].length) + username + ' ';
  const payload = textBfCaret + curMsg.substring(caretPos);
  return payload;
}
