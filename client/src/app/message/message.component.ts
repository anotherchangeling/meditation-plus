import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { Message } from './message';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers';
import {
  selectMessageList,
  selectUsernames,
  selectLoading,
  selectLoadedPage,
  selectNoPagesLeft,
  selectInitiallyLoaded,
  selectMessages
} from 'app/message/reducers/message.reducers';
import { take, filter, map, tap } from 'rxjs/operators';
import { NgZone } from '@angular/core';
import {
  LoadMessages,
  UpdateMessage,
  DeleteMessage
} from 'app/message/actions/message.actions';
import { LoadMessageEffect } from './effects/load-messages.effect';
import { selectAdmin } from '../auth/reducders/auth.reducers';

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: [
    './message.component.styl'
  ]
})
export class MessageComponent implements OnInit {

  @Output() loadingFinished: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('messageList', {read: ElementRef}) messageList: ElementRef;

  messages$: Observable<Message[]>;
  usernames$: Observable<string[]>;
  loading$: Observable<boolean>;
  admin$: Observable<boolean>;
  initiallyLoaded$: Observable<boolean>;
  page$: Observable<number>;
  noPagesLeft$: Observable<boolean>;
  lastScrollTop = 0;
  lastScrollHeight = 0;

  constructor(
    private store: Store<AppState>,
    private zone: NgZone,
    loadMessageEffect: LoadMessageEffect,
  ) {
    this.messages$ = store.select(selectMessageList);
    this.usernames$ = store.select(selectUsernames);
    this.loading$ = store.select(selectLoading);
    this.initiallyLoaded$ = store.select(selectInitiallyLoaded);
    this.page$ = store.select(selectLoadedPage);
    this.noPagesLeft$ = store.select(selectNoPagesLeft);
    this.admin$ = store.select(selectAdmin);

    // Load first page, if no messages have been loaded
    store.select(selectMessages).pipe(
      take(1),
      filter(val => val.messages.length === 0)
    )
    .subscribe(() => store.dispatch(new LoadMessages(0)));

    // scroll to last message when clicking on "load more"
    loadMessageEffect.load$
      .pipe(
        map(() => localStorage.getItem('lastMessageId')),
        filter(val => !!val),
        map(id => document.getElementById('message-' + id)),
        tap(() =>
          Array.from(document.querySelectorAll('load-more-separator')).forEach(el => el.classList.remove('load-more-separator'))
        ),
        tap(el => el.classList.add('load-more-separator'))
      )
      .subscribe(el => el.scrollIntoView());
  }

  ngOnInit() {
    this.registerScrolling();

    this.messages$
      .pipe(
        filter(() => this.lastScrollTop + 5 >= this.lastScrollHeight
        - this.messageList.nativeElement.offsetHeight)
      )
      .subscribe(() => this.scrollToBottom());

    this.loadingFinished.emit();
  }

  delete(message: Message) {
    this.store.dispatch(new DeleteMessage(message));
  }

  update(message: Message) {
    this.store.dispatch(new UpdateMessage(message));
  }

  loadNextPage() {
    this.page$.pipe(take(1))
      .subscribe(page => this.store.dispatch(new LoadMessages(page + 1)));
  }

  /**
   * Registers scrolling as observable. Running this outside of zone to ignore
   * a change detection run on every scroll event. This resulted in a huge performance boost.
   */
  registerScrolling() {
    this.zone.runOutsideAngular(() => {
      let scrollTimer = null;
      this.messageList.nativeElement.addEventListener('scroll', () => {
        if (scrollTimer !== null) {
          clearTimeout(scrollTimer);
        }
        scrollTimer = setTimeout(() => {
          this.lastScrollHeight = this.messageList.nativeElement.scrollHeight;
          this.lastScrollTop = this.messageList.nativeElement.scrollTop;
        }, 150);
      }, false);
    });
  }

  public scrollToBottom() {
    window.setTimeout(() => {
      this.messageList.nativeElement.scrollTop = this.messageList.nativeElement.scrollHeight;
    }, 10);
  }

  trackById(_index, item: Message) {
    return item._id;
  }
}
