import { Component, Input, OnChanges } from '@angular/core';
import { UserService } from '../../user/user.service';
import * as moment from 'moment-timezone';
import { Profile } from '../profile';

@Component({
  selector: 'profile-charts',
  templateUrl: './profile-chart.component.html',
  styleUrls: ['./profile-chart.component.styl']
})
export class ProfileChartComponent implements OnChanges {

  @Input() data;
  @Input() user: Profile;

  currentMonth = moment().format('MMMM YYYY');
  maxMonthOffset = Number.MAX_SAFE_INTEGER;

  chartData = {
    week: { labels: [], datasets: [] },
    month: { labels: [], datasets: [] },
    year: { labels: [], datasets: [] }
  };

  chartOptions = {
    animations: true,
    maintainAspectRatio: false,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    cubicInterpolationMode: 'monotone'
  };

  monthOffset = 0;

  constructor(private userService: UserService) {
  }

  /**
   * Helper function for left-shifting an array. This will
   * remove the first n elements and add it to the end of
   * the array. Example:
   *
   * rotateArray([4,5,1,2,3], 2) => [1,2,3,4,5]
   *
   * @param  {any[]}  arr   Array of elements
   * @param  {number} size  Number of first elements to move
   * @return {any[]}        Transformed array
   */
  rotateArray(arr: any[], size: number): any[] {
    return size > 0 && size % 1 === 0
      ? arr.slice(size % arr.length, arr.length).concat(arr.slice(0, size % arr.length))
      : arr;
  }

  /**
   * Resets chart data
   */
  resetChart(): void {
    this.monthOffset = 0;
    this.currentMonth = moment().format('MMMM YYYY');
    this.chartData = {
      year: { labels: [], datasets: [] },
      month: { labels: [], datasets: [] },
      week: { labels: [], datasets: [] }
    };
  }

  /**
   * Loads a chart by name.
   *
   * @param chartName Name of chart = key in 'chartData', 'chartLabels' and 'data'
   */
  loadChart(chartName: string): void {
    if (!this.data || !chartName || !this.data[chartName]) {
      return;
    }

    let labels, shiftSize;

    // determine x-axis labels and shift size (for correct displaying of
    // data, i.e. week starts at Monday not Sunday) for current chart
    switch (chartName) {
      case 'year':
        labels = moment.monthsShort();
        shiftSize = moment().month() + 1;
        break;
      case 'month':
        // range from first to last day of current month
        const mom = moment().subtract(this.monthOffset, 'months');
        this.currentMonth = mom.format('MMMM YYYY');
        labels = new Array(mom.daysInMonth())
          .fill(0)
          .map((_, i) => (i + 1).toString());

        shiftSize = 0;
        break;
      case 'week':
        labels = moment.weekdaysShort();
        shiftSize = 1;
        break;
      default:
        return;
    }

    const dataWalking = {
      data: new Array(labels.length).fill(0),
      label: 'Minutes walking',
      backgroundColor: chartName !== 'month' ?  'rgba(81, 33, 255, 0.4)' : 'transparent',
      borderColor: '#58b2ef',
      fill: chartName !== 'month'
    };

    const dataSitting = {
      data: new Array(labels.length).fill(0),
      label: 'Minutes sitting',
      backgroundColor: chartName !== 'month' ? 'rgba(255, 33, 81, 0.4)' : 'transparent',
      borderColor: '#ff6384',
      fill: chartName !== 'month'
    };

    this.data[chartName].map(x => {
      dataWalking.data[x._id - 1] = x.walking;
      dataSitting.data[x._id - 1] = x.sitting;
    });

    labels = this.rotateArray(labels, shiftSize);
    dataWalking.data = this.rotateArray(dataWalking.data, shiftSize);
    dataSitting.data = this.rotateArray(dataSitting.data, shiftSize);

    this.chartData[chartName].labels = labels;
    this.chartData[chartName].datasets.push(dataWalking);
    this.chartData[chartName].datasets.push(dataSitting);
  }

  loadNextMonth(backwards: boolean = true): void {
    if (!this.user || !this.user._id) {
      return;
    }

    this.monthOffset = backwards
      ? this.monthOffset + 1
      : Math.max(0, this.monthOffset - 1);

    this.userService.getProfileStats(this.user._id, this.monthOffset).subscribe(res => {
      // declaration like this is important to trigger chart.js change detection
      this.chartData['month'] = {
        datasets: [],
        labels: []
      };
      this.data['month'] = res;
      this.loadChart('month');
    });
  }

  ngOnChanges() {
    // Update charts
    if (this.data) {
      this.resetChart();
      Object.keys(this.data).map(c => this.loadChart(c));
    }

    if (this.user && this.user.createdAt) {
      this.maxMonthOffset = Math.ceil(
        moment
          .duration(moment.utc().diff(moment.utc(this.user.createdAt)))
          .asMonths()
      );
    }
  }
}
