import { Directive, ElementRef, Input, OnChanges } from '@angular/core';
import { ApiConfig } from '../../api.config';

@Directive({
  selector: '[avatar]'
})
export class AvatarDirective implements OnChanges {
  @Input() size: 40 | 80 | 160 = 160;
  @Input() username = '';
  @Input() token = '';

  constructor(private elementRef: ElementRef) {}

  ngOnChanges() {
    if (!this.username || !this.token) {
      this.elementRef.nativeElement.srcset =
        `/assets/img/profile-${this.size}.jpg 1x,
         /assets/img/profile-${this.size * 2}.jpg 2x,`;

      // Fallback for older browsers
      this.elementRef.nativeElement.src = `/assets/img/profile-${this.size}.jpg`;

      return;
    }

    // Different sizes for HiDPI support
    this.elementRef.nativeElement.srcset =
    `${ApiConfig.url}/public/${this.username}-${this.token}-${this.size}.jpeg 1x,
     ${ApiConfig.url}/public/${this.username}-${this.token}-${this.size * 2}.jpeg 2x,`;

    // Fallback for older browsers
    this.elementRef.nativeElement.src =
      `${ApiConfig.url}/public/${this.username}-${this.token}-${this.size}.jpeg`;
  }
}
