import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BadgeComponent } from './badge/badge.component';
import { FlagComponent } from './flag/flag.component';
import { OfflineMeditationComponent } from './offline-meditation/offline-meditation.component';
import { ProfileFormComponent } from './profile-form.component';
import { ProfileComponent } from './profile.component';
import { ProfileChartComponent } from './profile-chart/profile-chart.component';
import { UserModule } from '../user';
import { SharedModule } from '../shared';
import { MomentModule } from 'ngx-moment';
import { MeditatedRecentlyDirective } from './meditated-recently.directive';
import { ChartModule } from 'primeng/chart';
import { ProfileDeleteDialogComponent } from './profile-delete-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    ChartModule,
    RouterModule,
    FormsModule,
    UserModule,
    MomentModule
  ],
  entryComponents: [
    ProfileDeleteDialogComponent
  ],
  declarations: [
    ProfileFormComponent,
    ProfileDeleteDialogComponent,
    ProfileComponent,
    OfflineMeditationComponent,
    FlagComponent,
    BadgeComponent,
    ProfileChartComponent,
    MeditatedRecentlyDirective
  ],
  exports: [
    ProfileFormComponent,
    ProfileComponent,
    ProfileDeleteDialogComponent,
    OfflineMeditationComponent,
    FlagComponent,
    BadgeComponent,
    ProfileChartComponent,
    MeditatedRecentlyDirective
  ]
})
export class ProfileModule { }
