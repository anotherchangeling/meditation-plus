import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared';
import { ProfileModule } from '../profile';
import { AppointmentComponent } from './appointment.component';

const routes: Routes = [
  { path: '', component: AppointmentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ProfileModule
  ],
  declarations: [
    AppointmentComponent
  ],
  exports: [
    AppointmentComponent
  ]
})
export class AppointmentModule { }
