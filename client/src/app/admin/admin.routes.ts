import { CommitmentAdminComponent } from './commitment/commitment-admin.component';
import { CommitmentFormComponent } from './commitment/commitment-form.component';
import { AppointmentAdminComponent } from './appointment/appointment-admin.component';
import { AppointmentFormComponent } from './appointment/appointment-form.component';
import { UserAdminFormComponent } from './user/user-admin-form.component';
import { UserAdminComponent } from './user/user-admin.component';
import { TestimonialAdminComponent } from './testimonial/testimonial-admin.component';
import { MeetingAdminComponent } from './meeting/meeting-admin.component';
import { BroadcastAdminComponent } from './broadcast/broadcast-admin.component';
import { BroadcastFormComponent } from './broadcast/broadcast-form.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { AdminIndexComponent } from './admin-index.component';
import { Routes } from '@angular/router';
import { VideoSuggestionsComponent } from './video-suggestions/video-suggestions.component';
import { AdminComponent } from './admin.component';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', component: AdminIndexComponent },
      { path: 'broadcasts', component: BroadcastAdminComponent },
      { path: 'broadcasts/new', component: BroadcastFormComponent },
      { path: 'broadcasts/:id', component: BroadcastFormComponent },
      { path: 'commitments', component: CommitmentAdminComponent },
      { path: 'commitments/new', component: CommitmentFormComponent },
      { path: 'commitments/:id', component: CommitmentFormComponent },
      { path: 'appointments', component: AppointmentAdminComponent },
      { path: 'appointments/new', component: AppointmentFormComponent },
      { path: 'appointments/:id', component: AppointmentFormComponent },
      { path: 'users', component: UserAdminComponent },
      { path: 'users/new', component: UserAdminFormComponent },
      { path: 'users/:id', component: UserAdminFormComponent },
      { path: 'testimonials', component: TestimonialAdminComponent },
      { path: 'testimonials/:id', component: TestimonialAdminComponent },
      { path: 'testimonials', component: TestimonialAdminComponent },
      { path: 'testimonials/:id', component: TestimonialAdminComponent },
      { path: 'testimonials/review', component: TestimonialAdminComponent },
      { path: 'analytics', component: AnalyticsComponent },
      { path: 'video-suggestions', component: VideoSuggestionsComponent },
      { path: 'meeting', component: MeetingAdminComponent }
    ]
  }
];
