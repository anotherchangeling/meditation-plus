import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppointmentService } from '../../appointment';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'appointment-form',
  templateUrl: './appointment-form.component.html'
})
export class AppointmentFormComponent {

  appointment = {};
  loading = false;
  errorMsg = '';

  constructor(
    private appointmentService: AppointmentService,
    private router: Router,
    private snackbar: MatSnackBar
  ) {
  }

  submit() {
    this.loading = true;
    this.appointmentService
      .save(this.appointment)
      .subscribe(
        () => {
          this.snackbar.open('The appointment has been saved successfully.');
          this.router.navigate(['/admin/appointments']);
        },
        err => {
          this.errorMsg = err.error;
          this.loading = false;
        },
        () => this.loading = false
      );
  }
}
