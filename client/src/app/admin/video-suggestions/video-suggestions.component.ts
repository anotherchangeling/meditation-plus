import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { VideoSuggestionsService } from './video-suggestions.service';
import { MatSnackBar } from '@angular/material';
import { DialogService } from '../../dialog/dialog.service';
import { filter, concatMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'app-video-suggestions',
  templateUrl: './video-suggestions.component.html',
  styleUrls: ['./video-suggestions.component.scss']
})
export class VideoSuggestionsComponent {

  loading = false;
  suggestions$: Observable<any>;

  constructor(
    private service: VideoSuggestionsService,
    private snackbar: MatSnackBar,
    private dialog: DialogService,
    private store: Store<AppState>
  ) {
    this.suggestions$ = this.service.getAll();
  }

  accept(suggestion) {
    this.loading = true;
    this.service.accept(suggestion).subscribe(
      () => {
        this.suggestions$ = this.service.getAll();
        this.snackbar.open('The suggestions has been accepted successfully.');
        this.loading = false;
      },
      error => this.store.dispatch(new ThrowError({ error })),
      () => this.loading = false
    );
  }

  delete(suggestion) {
    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      tap(() => this.loading = true),
      concatMap(() => this.service.delete(suggestion))
    ).subscribe(
      () => {
        this.suggestions$ = this.service.getAll();
        this.snackbar.open('The suggestions has been deleted successfully.');
        this.loading = false;
      },
      error => this.store.dispatch(new ThrowError({ error })),
      () => this.loading = false
    );
  }
}
