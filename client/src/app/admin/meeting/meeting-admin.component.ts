import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../shared';

@Component({
  selector: 'meeting-admin',
  templateUrl: './meeting-admin.component.html'
})
export class MeetingAdminComponent implements OnInit {

  constructor(
    private settingsService: SettingsService
  ) { }

  newKey = '';
  newValue = '';

  settings: any;

  ngOnInit() {
    this.loadSettings();
  }

  loadSettings(): void {
    this.settingsService
      .get()
      .subscribe(res => this.settings = res);
  }

  submit(evt): void {
    if (evt) {
      evt.preventDefault();
    }

    if (!this.newKey || !this.newValue || !this.settings) {
      return;
    }

    let replacements = [[this.newKey, this.newValue]];

    if (this.settings.hasOwnProperty('meetingAutocompletions') && this.settings.meetingAutocompletions instanceof Array) {
      replacements = replacements.concat(this.settings.meetingAutocompletions);
    }

    this.settingsService
      .set('meetingAutocompletions', replacements)
      .subscribe(() => {
        this.loadSettings();
        this.newKey = '';
        this.newValue = '';
      });
  }

  removeItem(index: number): void {
    if (!(this.settings && typeof this.settings['meetingAutocompletions'] !== 'undefined') || !(index >= 0)
      || index >= this.settings.meetingAutocompletions.length) {
      return;
    }

    const newReplacements = this.settings.meetingAutocompletions;
    newReplacements.splice(index, 1);

    this.settingsService
      .set('meetingAutocompletions', newReplacements)
      .subscribe(() => this.loadSettings());
  }

}
